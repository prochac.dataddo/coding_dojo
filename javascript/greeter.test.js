const Greeter = require('./greeter');

describe("Greeter", () => {
    const tests = [
        {name: "Alice", expected: "Hello, Alice!"},
        {name: "Bob", expected: "Hello, Bob!"},
    ];
    tests.forEach(test => {
        it("greets " + test.name, () => {
            const greeter = new Greeter()
            expect(greeter.greet(test.name)).toBe(test.expected);
        });
    });
});