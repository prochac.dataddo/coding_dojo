<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Greeter;

final class GreeterTest extends TestCase
{

    /**
     * @dataProvider testGreetDataProvider
     */
    public function testGreet($name, $want): void
    {
        $greeter = new Greeter;
        $got = $greeter->greet($name);
        $this->assertSame($want, $got);
    }

    /**
     * @return array
     */
    public static function testGreetDataProvider(): array
    {
        return [
            [
                'name' => 'Alice',
                'want' => 'Hello, Alice!'
            ],
            [
                'name' => 'Bob',
                'want' => 'Hello, Bob!'
            ]
        ];
    }
}