#!/usr/bin/php
<?php

if (php_sapi_name() !== 'cli') {
    exit;
}

require __DIR__ . '/vendor/autoload.php';

use App\Greeter;

$greeter = new Greeter();
echo $greeter->greet('Alice') . PHP_EOL;