import {Greeter} from './greeter';

const greeter = new Greeter();
console.log(greeter.greet("Alice"));