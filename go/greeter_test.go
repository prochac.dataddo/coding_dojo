package main

import "testing"

func TestGreeter_Greet(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "Alice",
			want: "Hello, Alice!",
		},
		{
			name: "Bob",
			want: "Hello, Bob!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gr := Greeter{}
			if got := gr.Greet(tt.name); got != tt.want {
				t.Errorf("Greet() = %v, want %v", got, tt.want)
			}
		})
	}
}
