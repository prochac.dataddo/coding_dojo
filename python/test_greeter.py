import unittest
from greeter import *


class TestGreeter(unittest.TestCase):
    def test_anonymize_emails(self):
        test_cases = [
            {'name': 'Alice', 'expected_result': 'Hello, Alice!'},
        ]
        for t in test_cases:
            greeter = Greeter()
            result = greeter.greet(t['name'])
            self.assertEqual(result, t['expected_result'])


if __name__ == '__main__':
    unittest.main()
