# Coding Dojo template

## Introduction

Pick your coding partner, language and start coding!

You should choose a language that you are not familiar with.
One, or both, of you should be new to the language.
Start with the test first and then let your partner implement the solution.
Then switch roles and repeat the process.
The tests should be written as table tests, i.e. one test case per row in the table.
The test templates are prepared for you in language specific examples.

The goal is to learn a new language and to learn how to write tests in a TDD fashion.

The flow is as follows:

1) A writes a test
2) B implements the solution
3) B writes a test
4) A implements the solution
5) repeat...

## How to use

### Go

Run test by executing the following command:

```bash
$ cd go
$ go test ./...
```

### JavaScript

Run test by executing the following command:

```bash
$ cd javascript
$ npm install # only once
$ npm test
```

### PHP

Run test by executing the following command:

```bash
$ cd php
$ composer install # only once
$ ./vendor/bin/phpunit tests
```

### Python

Run test by executing the following command:

```bash
$ cd python
$ python -m unittest
```

### TypeScript

Run test by executing the following command:

```bash
$ cd typescript
$ npm install # only once
$ npm test
```
